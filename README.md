# Lightning Hammer #

This is a datapack for minecraft java 1.16.4. Don't forget to use the resource pack.

### How to get the Lightning Hammer ###

* Step 1: Aquire an iron axe, a stack of _oak_ logs, a nether star and some armour as the lightning will damage you.
* Step 2: Throw the together on the ground.
* Step 4: Hold your Lightning Hammer and right-click.
* Enjoy the power of your Lightning Hammer.